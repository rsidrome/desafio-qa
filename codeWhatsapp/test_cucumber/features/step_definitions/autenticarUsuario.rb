require 'selenium-webdriver'
driver = Selenium::WebDriver.for :chrome

Dado("que o acesse o whatsapp via web") do
    driver.get 'https://web.whatsapp.com/'
    driver.manage.window.maximize
  end
  
  Quando(/^clico no "([^"]*)" whatsapp web$/) do |botao|
    @driver.find_element(:link_text, botao).click
  end
  
  Entao(/^é exibido popup para pareamento com o celular$/) do
    wait = Selenium::WebDriver::Wait.new(:timeout => 5) # segundos
    begin
      element = wait.until { driver.find_element(:id => "search-title") }
      expect(element.text).to eq('Search Results for: agile')
    ensure
      driver.quit
    end
  end
  
  Dado("que acesso o whatsapp via celular") do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Quando(/^clico no "([^"]*)" com símbolo de reticências vertical$/) do |botao|
    @navegador.find_element(:link_text, botao).click
  end
  
  Quando("seleciono a opção Whatsapp web") do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Quando(/^clico no "([^"]*)" adicionar leitura de QRCode$/) do |botao|
    @navegador.find_element(:link_text, botao).click
  end
  
  Entao("é exibido popup para leitura do QRCode") do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Dado("que realize o pareamento para leitura do QRCode emitido pelo browser") do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Dado("realiza a leitura do QRCode com celular") do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Quando("leitura realizada") do
    pending # Write code here that turns the phrase above into concrete actions
  end
  
  Entao("é exibido no browser a lista dos contatos") do
    pending # Write code here that turns the phrase above into concrete actions
  end