
require 'selenium-webdriver'
driver = Selenium::WebDriver.for :chrome

Dado("que o contato exista") do
  pending # Write code here that turns the phrase above into concrete actions
end

Dado(/^clicando no "([^"]*)" Contato$/) do |botao|
  drievr.find_element(:link_text, botao).click
end

Dado(/^digito uma "([^"]*)" para o contato$/) do |contato|
  driver.find_element(:name, "contato").send_keys(contato)
end

Quando(/^clico no "([^"]*)" Enviar mensagem$/) do |botao|
  driver.find_element(:link_text, botao).click
end

Então(/^verei dois "([^"]*)" de sucesso que foi enviado a mensagem$/) do |mensagem|
  expect(@navegador.find_element(:css,".alert").text).to eq(mensagem)
  driver.quit
end
