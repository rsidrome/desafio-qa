# language: pt

Funcionalidade: Buscar Contato
    Acessando whatsapp pelo aplicativo
    Levando em consideração que minha lista é extensa e será preciso realizar uma busca

Cenario: Realizando uma Busca no listView
    
    Dado que acesse o aplicativo whatsapp
        E clicando no input Search da navbar do aplicativo
        E digito o nome do "Contato"
    Quando seleciono o "Contato" de acordo com a busca
        E clico no nome do Contato
    Então é exibido o perfil do "Contato"
