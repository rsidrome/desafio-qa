# language: pt

Funcionalidade: Enviar Mensagem
    Acessando whatsapp pelo aplicativo
    Levando em consideração que já selecionei meu contato

Cenario: Enviando mensagem para meu contato
    
    Dado que o contato exista
        E clicando no Contato
        E digito uma "Mensagem" para o contato
    Quando clico no botão Enviar mensagem
    Então verei dois "ticks" de sucesso que foi enviado a mensagem