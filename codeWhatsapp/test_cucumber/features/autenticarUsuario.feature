# language: pt

Funcionalidade: Autenticar Usuario
    Autenticando usuário whatsapp via pelo celular/web

Cenario: Acessando whatsapp via browser
    
    Dado que o acesse o whatsapp via web
        E digito o endereço do whatsapp no browser 
    Quando clico no botão whatsapp web
    Entao é exibido popup para pareamento com o celular

Cenario: Acessando whatsapp via celular
    
    Dado que acesso o whatsapp via celular 
    Quando clico no botão com símbolo de reticências vertical
        E seleciono a opção Whatsapp web
    Quando clico no botão adicionar leitura de QRCode
    Entao é exibido popup para leitura do QRCode

Cenario: Autenticando Usuário QRCode
    
    Dado que realize o pareamento para leitura do QRCode emitido pelo browser
        E realiza a leitura do QRCode com celular 
    Quando leitura realizada
    Entao é exibido no browser a lista dos contatos
