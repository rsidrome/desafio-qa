var chai = require('chai')
  , Checkout = require('./checkout')
  , Precificacao = require('./pricingRules');

chai.should();

describe('testCheckout.js', function() {

  describe('Quando o item A é escaneado uma vez e o item B é escaneado uma vez', function() {
    it('deve retornar o preço total de 80', function() {
      var checkout = new Checkout(Precificacao);

      checkout.scan('A');
      checkout.scan('B');

      checkout.getTotal().should.equal(80);
    });
  });

  describe('Quando o item A é digitalizado três vezes e o item B é digitalizado uma vez', function() {
    it('deve retornar o preço total de 160', function() {
      var checkout = new Checkout(Precificacao);

      checkout.scan('A');
      checkout.scan('A');
      checkout.scan('A');
      checkout.scan('B');

      checkout.getTotal().should.equal(160);
    });
  });

  describe('Quando o item A é digitalizado quatro vezes e o item B é digitalizado duas vezes', function() {
    it('deve retornar o preço total de 225', function() {
      var checkout = new Checkout(Precificacao);

      checkout.scan('A');
      checkout.scan('A');
      checkout.scan('A');
      checkout.scan('A');
      checkout.scan('B');
      checkout.scan('B');

      checkout.getTotal().should.equal(225);
    });
  });
});