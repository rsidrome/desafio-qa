var chai = require('chai')
  , Precificacao = require('./precificacao');

chai.should();

describe('testCheckout.js', function() {

  describe('Quando o item A é escaneado uma vez', function() {
    it('deve retornar o preço para o item A', function() {
        Precificacao['A'](1).should.equal(50);
    });
  });

  describe('Quando o item A é digitalizado duas vezes', function() {
    it('deve retornar o preço para 2 itens A', function() {
        Precificacao['A'](2).should.equal(100);
    });
  });

  describe('Quando o item A é digitalizado três vezes', function() {
    it('deve retornar o preço do pacote para 3 itens A', function() {
        Precificacao['A'](3).should.equal(130);
    });
  });

  describe('Quando o item A é digitalizado quatro vezes', function() {
    it('deve retornar o preço do pacote para 3 itens A mais 1 preço de item único', function() {
        Precificacao['A'](4).should.equal(180);
    });
  });

  describe('Quando o item B é escaneado uma vez', function() {
    it('deve retornar o preço para o item B', function() {
        Precificacao['B'](1).should.equal(30);
    });
  });

  describe('Quando o item B é digitalizado cinco vezes', function() {
    it('deve retornar o preço do pacote para 4 itens B mais 1 preço de item único', function() {
        Precificacao['B'](5).should.equal(120);
    });
  });
});