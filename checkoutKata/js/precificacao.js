module.exports = {
    'A':bundledPreco({singlePreco:50, bundledPreco: 130, bundledTamanho: 3}),
    'B': bundledPreco({singlePreco: 30, bundledPreco: 45, bundledTamanho: 2}), 
    'C': singlePreco({singlePreco: 30})
}

function bundledPreco(options){
    return function(){
        var bundledQuantidade = (quantidade - quantidade % options.bundledTamanho) / options.bundledTamanho;
        quantidade = quantidade % options.bundledTamanho;
        return bundledQuantidade * options.bundledPrice + quantidade * options.singlePreco;
    }
}

function singlePreco(options){
    return function(quantidade){
        return quantidade * options.singlePreco
    }
}