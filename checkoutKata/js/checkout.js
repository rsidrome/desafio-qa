module.exports = Checkout;

function Checkout(preco){
    this.preco = preco;
    this.items = {}
}

Checkout.prototype.scan = function(item){
    this.items[item] = this.items[item] + 1
}

Checkout.prototype.getTotal = function(){
    var total = 0
    , self = this;
  _.forIn(this.items, function(quantidade, item) {
       total += self.preco[item](quantidade)
    });
  return total;
};
